/** @constant {Array} bodyMassIndex - Array with the values of the BMI and their respective classification. */

const bodyMassIndex = [
  { value: 18.5, message: "abaixo do peso" },
  { value: 24.9, message: "peso normal" },
  { value: 29.9, message: "sobrepeso" },
  { value: 34.9, message: "obesidade grau 1" },
  { value: 39.9, message: "obesidade grau 2" },
  { value: Infinity, message: "obesidade grau 3" }
];

/**
* Calculates the BMI of a person.
* @param {Event} e - Event object.
*/

function calculateBMI(e) {

  console.clear();

  console.log("Clique no botão calcular");

  let weight = parseFloat(document.querySelector("#weight")?.value);

  let height = parseFloat(document.querySelector("#height")?.value);

  if ((isNaN(weight) || isNaN(height)) || (height === 0 || weight === 0)) {
    alert("Fill all fields correctly!");

    return;
  }

  console.log('Calculando IMC...');

  let bmi = (weight / (height * height)).toFixed(2);

  let result = bodyMassIndex.find((item) => parseFloat(bmi) < item.value)?.message || "classification not found";

  let divResultado = document.querySelector("#result");

  if (divResultado) {
    divResultado.textContent = `Your ims is ${bmi}, so you are ${result}`;
    console.log(`IMC = ${bmi} \n${result}`);
  }

  e.preventDefault();
};

try {
  document.querySelector("#calcular")?.addEventListener("click", calculateBMI);
} catch (err) {
  console.log(err);
}
