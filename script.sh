#!/bin/bash

# System update and upgrade
sudo apt-get update && sudo sudo apt-get upgrade -y

# Install Docker
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

# Install Gitlab Runner
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

sudo apt-get install gitlab-runner

# Add user to docker group
sudo usermod -aG docker gitlab-runner

# Register Gitlab Runner
sudo gitlab-runner register --non-interactive \
--url https://gitlab.com/ \
--token glrt-6TYoaUUTaP2XES3Yw9yk \
--executor shell
